﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnObjektid
{
    static class Program
    {


        static void Main(string[] args)
        {
            

  
            Inimene ants = new Inimene { Nimi = "Ants", Vanus = 40 };

            Console.WriteLine(ants);
            Console.WriteLine(ants.GetVanus());
            ants.SetVanus(ants.GetVanus()+5);
            Console.WriteLine(ants);

            Console.WriteLine(ants.Vanus);
            ants.Vanus += 5;
            Console.WriteLine(ants);

            
        }
    }

 
    class Inimene
    {

        static int _InimesteArv = 0;
        static List<Inimene> Nimekiri = new List<Inimene>();

        private int Number = 0;
        private string _Nimi;  // private field
        private int _Vanus; // public field

        // konstruktor on meetod, mille nimi == klassi nimi

        public Inimene() : this("tundmatu", 10) { } // 0 - parameetriteta
        public Inimene(int vanus) : this("tundmatu", vanus) { } // 2
        public Inimene(string nimi) : this(nimi, 11) { } // 3

//        public static int InimesteArv { get { return _InimesteArv; } }
//        public static int InimesteArv { get => _InimesteArv; } 
        public static int InimesteArv => _InimesteArv; 

        public Inimene(string nimi, int vanus) // 1 - kahe parameetriga
        {
            Nimekiri.Add(this);
            this.Number = ++_InimesteArv;

            _Nimi = nimi;
            _Vanus = vanus;
        }

        public int InimeseNumber()
        {
            return this.Number;
        }

        //public int AnnaVanus() { return this.Vanus; }
        public int GetVanus() => this._Vanus;

        //public void MuudaVanust(int uusvanus)
        //{
        //    if (uusvanus > Vanus) Vanus = uusvanus;
        //}
        
        public void SetVanus(int uusVanus) => 
            _Vanus = uusVanus > _Vanus ? uusVanus : _Vanus;

        public int Vanus
        {
            get => _Vanus; 
            set =>_Vanus = value > _Vanus ? value : _Vanus; 
        }

          public static void TrykiInimesed()
        {
            foreach (Inimene i in Nimekiri) Console.WriteLine(i);
        }


        public string Nimi  // public property
        {
            get { return _Nimi; }
            set
            {
                _Nimi = String.Join
                    (" ",
                    value
                        .Split(' ')
                        .Select
                            (
                                x => 
                                x == "" ? "" : 
                                x.Substring(0, 1).ToUpper() + 
                                x.Substring(1).ToLower()
                            )
                    );
            }
        }

        // objekti meetod
        // objekti meetodil on olemas this - lokaalne muutuja
        // this TÄHISTAB seda asja, mis on meetodi väljakutsumisel
        // selle punkti ees
        public void Tryki()
        {
            Console.WriteLine(this);
        }
        // staatiline meetod
        public static void StaatilineTryki(Inimene kes)
        {
            Console.WriteLine(kes);
        }


        public override string ToString() // public function (overrided)
        {
            return $"({Number:000}) Inimene nimega {Nimi} ja vanusega {_Vanus}";
        }
    }
}
